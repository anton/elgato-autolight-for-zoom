#!/bin/zsh

# Set ELGATO_RING_LIGHT_IP in your ~/.zshrc file
# export ELGATO_RING_LIGHT_IP="x.x.x.x"

ACTIVE_CALL=0
LIGHT_ON=0

log_message() {
    echo "[$(date '+%Y-%m-%d %H:%M:%S')] $1"
}

check_power_source() {
    if [ "$(pmset -g ps | head -1)" = "Now drawing from 'AC Power'" ]; then
        return 0
    else
        return 1
    fi
}

turn_light_on() {
    log_message "Turning light on"
    curl -s -o /dev/null --location --max-time 1 --request PUT "http://$ELGATO_RING_LIGHT_IP:9123/elgato/lights" \
         --header 'Content-Type: application/json' \
         --data-raw '{"lights":[{"on":1}],"numberOfLights":1}'
    LIGHT_ON=1
}

turn_light_off() {
    log_message "Turning light off"
    curl -s -o /dev/null --location --max-time 1 --request PUT "http://$ELGATO_RING_LIGHT_IP:9123/elgato/lights" \
         --header 'Content-Type: application/json' \
         --data-raw '{"lights":[{"on":0}],"numberOfLights":1}'
    LIGHT_ON=0
}

log_message "Script started"

while true; do
    # Check if plugged into power
    if check_power_source; then
        # Watch for any active Zoom call processes
        IS_ACTIVE_CALL=$(pgrep -fi "/Applications/zoom.us.app/Contents/Frameworks/cpthost.app/Contents/MacOS/CptHost" | wc -l)

        # Turn on the light if a Zoom call has started
        if [ "$IS_ACTIVE_CALL" -eq 1 ] && [ "$ACTIVE_CALL" -eq 0 ]; then
            log_message "Zoom call detected"
            ACTIVE_CALL=1
            if [ "$LIGHT_ON" -eq 0 ]; then
                turn_light_on
            else
                log_message "Light already on"
            fi
        # Turn off the light if a Zoom call has ended
        elif [ "$IS_ACTIVE_CALL" -eq 0 ] && [ "$ACTIVE_CALL" -eq 1 ]; then
            log_message "Zoom call ended"
            ACTIVE_CALL=0
            if [ "$LIGHT_ON" -eq 1 ]; then
                turn_light_off
            else
                log_message "Light already off"
            fi
        fi
    else
        # Turn off the light if on battery power
        if [ "$LIGHT_ON" -eq 1 ]; then
            log_message "Switched to battery power"
            turn_light_off
        fi
        if [ "$ACTIVE_CALL" -eq 1 ]; then
            log_message "Call ended or switched to battery power"
            ACTIVE_CALL=0
        fi
    fi

    sleep 5
done