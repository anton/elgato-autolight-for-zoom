<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.gitlab.elgato-autolight-for-zoom</string>
    <key>ProgramArguments</key>
    <array>
        <string>{path}/elgato-autolight-for-zoom/zoom-ring-light.sh</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>StandardOutPath</key>
    <string>{path}/Library/Logs/elgato-autolight-for-zoom.log</string>
    <key>StandardErrorPath</key>
    <string>{path}/Library/Logs/elgato-autolight-for-zoom.log</string>
    <key>EnvironmentVariables</key>
    <dict>
        <key>ELGATO_RING_LIGHT_IP</key>
        <string>{x.x.x.x}</string>
    </dict>
</dict>
</plist>